import re;
import random;
import time;
import math;

wall_tile = '#'
empty_tile = ' '
goal_tile = '.'

(N, E, S, W) = (6, 7, 8, 9)
directions = [N, E, S, W]

stat_explore_count = 0
stat_exploit_count = 0
best_scored = None
best_score = 0

def raw_input(i = "") :
    return input(i)

def millitime() :
    return int(round(time.time() * 1000))

def move_point(point, direction) :
    if direction == N:
        return (point[0], point[1] - 1)
    elif direction == E:
        return (point[0] + 1, point[1])
    if direction == S:
        return (point[0], point[1] + 1)
    elif direction == W:
        return (point[0] - 1, point[1])
    else:
        return point



class Board :
    def __init__(self, width, height, tiles, boxes, ppos) :
        self.width = width
        self.height = height
        self.tiles = tiles # [(tile, )] # maybe in the future add the information of which neighbor directions are empty
        self.boxes = boxes

        if ppos == None :
            self.ppos_reach = None
        elif ppos.__class__.__name__ == "tuple" :
            self.ppos_reach = self.reachable_from(ppos)
        elif ppos.__class__.__name__ == "set" :
            self.ppos_reach = ppos
        else:
            raise ValueError("ppos argument in Board constructor must be either tuple or set. Was " + str(ppos.__class__.__name__))

    def tiles_index(self, p) :
        return p[1] * self.width + p[0]

    def tiles_coords(self, index) :
        x = index % self.width
        y = int(index / self.width)
        return (x, y)

    def test_coords(self, p) :
        return p[0] >= 0 and p[0] < self.width and p[1] >= 0 and p[1] < self.height

    def tile_at(self, p) :
        if self.test_coords(p) :
            return self.tiles[self.tiles_index(p)]
        else :
            return wall_tile

    def new_ppos(self, ppos) :
        if ppos.__class__.__name__ == "tuple" :
            self.ppos_reach = self.reachable_from(ppos)
        elif ppos.__class__.__name__ == "set" :
            self.ppos_reach = ppos
        else:
            raise ValueError("ppos argument in new_ppos must be either tuple or set. Was " + str(ppos.__class__.__name__))

    def move_box(self, box_index, dir) :
        new_pos = move_point(self.boxes[box_index], dir)
        new_ppos = move_point(new_pos, dir)
        if (not new_pos in self.ppos_reach) or (not new_ppos in self.ppos_reach) :
            raise ValueError("Moving a box in a way that is not feasable with current player reach")
        self.boxes[box_index] = new_pos
        self.ppos_reach = self.reachable_from(new_ppos)

    def reachable_from(self, p) :
        reach = set([p])
        queue = [p]
        while len(queue) > 0 :
            p = queue.pop()
            for dir in directions :
                neigh = move_point(p, dir)
                if self.tile_at(neigh) != wall_tile and (not neigh in self.boxes) and not neigh in reach :
                    reach.add(neigh)
                    queue.append(neigh)
        return reach

    def clone(self) :
        new_reach = self.ppos_reach
        if new_reach != None :
            new_reach = set(new_reach)
        return Board(self.width, self.height, self.tiles[:], self.boxes[:], new_reach)


    def str_with_goals(self, goals) :
        s = ""
        for i, t in enumerate(self.tiles) :
            if i % self.width == 0 :
                s += "\n "
            coords = self.tiles_coords(i)
            if goals != None and coords in self.boxes and coords in goals :
                s += '0'
            elif goals != None and coords in goals :
                s += "."
            elif coords in self.boxes :
                s += "o"
            elif self.ppos_reach != None and (coords in self.ppos_reach) :
                s += "_"
            else:
                s += str(t)
        if self.ppos_reach == None :
            s += "\nNo Player yet"
        else:
            s += "\nPlayer reach : " + str(self.ppos_reach)
        return s

    def __str__(self) :
        return self.str_with_goals(None)

    def __eq__(self, other) :
        return other.__class__ == Board and self.width == other.width and self.height == other.height and self.tiles == other.tiles and self.boxes == other.boxes and self.ppos_reach == other.ppos_reach

    def __ne__(self, other) :
        return not self.__eq__(other)

    def __hash__(self) :
        if self.ppos_reach == None :
            frozen = None
        else:
            frozen = frozenset(self.ppos_reach)
        return hash((self.width, self.height, tuple(self.tiles), tuple(self.boxes), frozen))


class PlaceEmpty:
    def __init__(self, position):
        self.pos = position

    def next_state(self, state):
        new_board = state.board.clone()
        new_board.tiles[new_board.tiles_index(self.pos)] = empty_tile
        new_exp = set(state.expandables)
        new_exp.discard(self.pos)
        new_empt = set(state.empties)
        new_empt.add(self.pos)
        for dir in directions :
            neigh_pos = move_point(self.pos, dir)
            if new_board.test_coords(neigh_pos) and new_board.tile_at(neigh_pos) == wall_tile:
                new_exp.add(neigh_pos)
        return state.next_state_with_board(new_board, new_exp, new_empt)

    def __str__(self):
        return "PlaceEmpty[" + str(self.pos) + "]"

class PlaceBox:
    def __init__(self, position):
        self.pos = position

    def next_state(self, state):
        new_board = state.board.clone()
        new_board.boxes.append(self.pos)
        new_empt = set(state.empties)
        new_empt.discard(self.pos)
        new_exp = set(state.expandables)
        return state.next_state_with_board(new_board, new_exp, new_empt)

    def __str__(self):
        return "PlaceBox[" + str(self.pos) + "]"

class PlacePlayer:
    def __init__(self, position):
        self.pos = position

    def next_state(self, state):
        new_board = state.board.clone()
        new_board.new_ppos(self.pos)
        new_state = state.next_state_with_board(new_board, state.expandables, state.empties, new_board.boxes)
        return new_state

    def __str__(self):
        return "PlacePlayer[" + str(self.pos) + "]"

class MoveBox:
    def __init__(self, box_index, direction):
        self.box_index = box_index
        self.dir = direction

    def next_state(self, state):
        new_board = state.board.clone()
        new_board.move_box(self.box_index, self.dir)
        new_state = state.next_state_with_board(new_board)
        new_state.move_seq.append(self)
        return new_state

    def __str__(self):
        return "MoveBox[" + str(self.box_index) + ", " + str(self.dir) + "]"

class EvaluatePlayout:
    def next_state(self, state):
        ns = state.next_state_with_board(state.board.clone())
        ns.is_leaf = True
        return ns;

    def __str__(self):
        return "EvaluatePlayout"


class State :
    def __init__(self, prev_state, board, expandables = set(), empties = set(), goal_positions = None, move_seq = []) :
        self.prev_state = prev_state
        self.children = []
        self.board = board
        self.goal_positions = goal_positions
        self.is_leaf = False
        self.expandables = expandables
        self.empties = empties
        self.visited_count = 1
        self.move_seq = move_seq
        self.score = self.heuristic()

    def next_state_with_board(self, board, expandables = None, empties = None, goal_positions = None) :
        if expandables == None:
            expandables = self.expandables
        if empties == None:
            empties = self.empties
        if goal_positions == None:
            goal_positions = self.goal_positions

        next_state = State(self, board, expandables, empties, goal_positions, self.move_seq[:])

        return next_state

    def next_actions(self):
        actions = set()
        if self.is_leaf :
            return actions
        if self.board.ppos_reach == None :
            if len(self.empties) == 0 and len(self.expandables) == 0 and len(self.board.boxes) == 0:
                (x, y) = (random.randint(0, self.board.width - 1),
                          random.randint(0, self.board.height - 1))
                actions.add(PlaceEmpty((x, y)))
            for p in self.expandables: # PlaceEmpty
                actions.add(PlaceEmpty(p))
            for p in self.empties: # PlaceBox and PlacePlayer
                actions.add(PlaceBox(p))
                actions.add(PlacePlayer(p)) # Maybe only place player once per reachable area
        else :
            for i, box_pos in enumerate(self.board.boxes) :
                for dir in directions :
                    new_pos = move_point(box_pos, dir)
                    new_ppos = move_point(new_pos, dir)
                    if (new_pos in self.board.ppos_reach and new_ppos in self.board.ppos_reach) :
                        actions.add(MoveBox(i, dir))
            actions.add(EvaluatePlayout())
        return actions

    def ucb(self, verbose = False):
        global stat_explore_count
        global stat_exploit_count
        if self.prev_state != None :
            exploi = self.ratio_score()
            explor = 1 * math.sqrt(2) * math.sqrt(math.log(self.prev_state.visited_count) / self.visited_count)
            if verbose :
                print("ucb - exploration : " + str(explor) + ", exploitation : " + str(exploi) )
            return explor + exploi
        else :
            return self.ratio_score()

    def heuristic(self, verbose = False) :

        w_count = 0
        g_count = 0
        b_count = 0
        distance = 0.5 + len(self.move_seq)

        if self.goal_positions == None:
            return 0.0

        for i, g in enumerate(self.goal_positions) :
            b = self.board.boxes[i]
            if b[1] == g[1] or b[0] == g[0] :
                continue
            for x in range(min(b[0], g[0]), max(b[0], g[0]) + 1) :
                for y in range(min(b[1], g[1]), max(b[1], g[1]) + 1) :
                    tile = self.board.tile_at((x, y))
                    if tile == wall_tile :
                        w_count += 1
                    if (x, y) in self.board.boxes :
                        index = self.board.boxes.index((x, y))
                        if self.goal_positions[index] == (x, y) :
                            w_count += 1
                            continue
                        b_count += 1
                    if (x, y) in self.goal_positions :
                        g_count += 1

            #b_count = max(0, b_count - 1)
            #g_count = max(0, g_count - 1)

        (a, b, c) = (0.5, 0.7, 1)
        congestion = (   a * w_count
                       + b * g_count
                       + c * (b_count + 0.5)
                       )

        terrain = 0
#        terrain = len(self.expandables)
        walls = []
        for empty in self.empties : # TODO verify that this actually works : is empty kept up to date after player is placed ?
            if empty in self.board.boxes : # TODO should never enter this right ?
                continue
            for dir in directions:
                neigh = move_point(empty, dir)
                if self.board.tile_at(neigh) == wall_tile :
                    walls.append(neigh)
        terrain = len(walls)

        terrain /= 1.0

#        score = math.sqrt(terrain * congestion * distance) / 3.0
        (tc, cc) = (0.5, 3.0)
#        score = (tc * terrain + cc * congestion) / (tc + cc)
#        score = math.sqrt(terrain * congestion) / 3.0
        score = math.sqrt(terrain * congestion) / 2.0

        if verbose :
            print("terrain : " + str(terrain) + ", congestion : " + str(congestion) + ", b_count : " + str(b_count) + ", g_count : " + str(g_count) + ", w_count : " + str(w_count) + ", distance : " + str(distance))
#
#        if score > 1.0 :
#            print("score is greater than 1.0 : " + str(score))
#            raw_input()
#
#        if score == 0 :
#            print("score is zero")
#            raw_input()

        return score

    def __str__(self) :
        s =  str(self.board.str_with_goals(self.goal_positions)) + "\n"
        s += str(self.score) + " (score)\n"
        s += str(self.heuristic(True)) + " (heuristic)\n"
        s += str(self.ratio_score()) + " (ratio_score)\n"
        s += "visited_count : " + str(self.visited_count) + "\n"
        s += "children_count : " + str(len(self.children)) + "\n"
        s += "ucb : " + str(self.ucb(True)) + "\n"
        s += "goal_positions : " + str(self.goal_positions) + "\n"
        s += "empties : " + str(self.empties) + "\n"
        s += "move_seq.count : " + str(len(self.move_seq))
        if self.is_leaf :
            s += "\nis Leaf"
        return s

    def ratio_score(self) :
        return self.score / self.visited_count

    def select(self, verbose = True) :
        global stat_explore_count
        global stat_exploit_count
        pass#--print("select")
        state = self
        if verbose :
            pass#--print(state)

#        exp_evol = 100000 # number of visits at which exploration probability is at 50% of exp_limit
#        exp_limit = 0.05 # limit of probability to explore as visited_count tends to inf
#        while len(state.children) > 0 :
#            max_state = state.children[0]
#            max_score = max_state.ratio_score()
#            if random.randint(0, int((1/exp_limit) * (1 / (((1 / exp_evol) * state.visited_count) ** 2 + 1)))) == 1 :
#                max_state = random.sample(state.children, 1)[0]
#                stat_explore_count += 1
#            else :
#                stat_exploit_count += 1
#                for c in state.children :
#                    c_score = c.ratio_score()
#                    if c_score > max_score :
#                        max_score = c_score
#                        max_state = c
#            state = max_state
#
#        return state

#        while len(state.children) > 0:
#            max_score = 0
#            max_child = state.children[0]
#            max_v_count = max_child.visited_count
#            min_v_count = max_child.visited_count
#            stat_exploit_count += 1
#            for c in state.children :
#                c_score = c.ratio_score()
#                if c.visited_count > max_v_count :
#                    max_v_count = c.visited_count
#                elif c.visited_count < min_v_count:
#                    min_v_count = c.visited_count
#                if c_score > max_score :
#                    max_score = c_score
#                    max_child = c
#            if max_v_count - min_v_count > 1 and random.uniform(0, 1.0) < 0.1 :
#                state = random.sample(state.children, 1)[0]
#                stat_explore_count += 1
#                continue
#
#            state = max_child
#            if verbose :
#                pass#--print(state)
#        return state
#
        while len(state.children) > 0:
            max_ucb = 0
            max_child = state.children[0]
            max_av_score = state.children[0].ratio_score()
            for c in state.children :
                c_ucb = c.ucb()
                if c_ucb > max_ucb :
                    max_ucb = c_ucb
                    max_child = c
                crs = c.ratio_score()
                if crs > max_av_score :
                    max_av_score = crs
            state = max_child
            if state.ratio_score() != max_av_score :
                stat_explore_count += 1
            else:
                stat_exploit_count += 1
            if verbose :
                pass#--print(state)

        pass# print(" > select() :\n" + str(state))
        return state

    def expand(self, visited) :
        actions = self.next_actions()
        for a in actions :
            n_s = a.next_state(self)
            if not n_s.board in visited : # GULLIBLE
                self.children.append(a.next_state(self))
                visited.add(n_s.board)
        pass#--print("in expand, visited_count is " + str(len(visited)))
        if len(actions) == 0 :
            pass#--print(self)
        pass# print(" > expand() : children_count = " + str(len(self.children)))

    def random_child(self, count = 1) :
        pass#--print("random_child.")
        if len(self.children) > 0 :
            if count == 1 :
                child = random.sample(self.children, 1)[0]
                pass# print(" > random_child()")
                return child
            else:
                count = min(count, len(self.children))
                return random.sample(self.children, count)
        else:
            return None

    def simulate(self, visited) :
        state = self
        sim_visited = set()
        pass# print(" > simulate() :\nSimulating from :\n" + str(state))
        pass#--print("simulate from\n" + str(state))
        while not state.is_leaf:
            actions = state.next_actions()
            while len(actions) > 0 : # Take an random action, get next_state, test for visited. If visited, discard action and try another one. If out of actions, stop.
                action = random.sample(actions, 1)[0]
                pass#--print("applying action : " + str(action))
                n_s = action.next_state(state)
                if not (n_s.board in visited or n_s.board in sim_visited) : # GULLIBLE
                    pass# print(action)
                    state = n_s
                    sim_visited.add(state.board)
                    break
                #print(action);
                #raw_input();
                actions.discard(action)
            if len(actions) == 0 :
                break
        pass#--print("Final state is\n" + str(state))

        pass# print("Leaf state :\n" + str(state))

        global best_score
        global best_scored
        if state.ratio_score() > best_score :
            pass# print("Updated best state")
            best_score = state.ratio_score()
            best_scored = state

        return state

    def backpropagate(self) :
        score = self.score
        pass# print(" > backpropagate() :")
        pass#--print("backpropagate. Score is " + str(score))
        state = self
        if (self.is_leaf or len(self.children) == 0) and self.visited_count > 2 :
            pass
#            return
        while state != None :
            state.score += self.ratio_score()
            state.visited_count += 1
            #print("state score becomes " + str(state.score) + ", its visited_count : " + str(state.visited_count) + " and ratio score : " + str(state.ratio_score()))
            state = state.prev_state


    def print_stacktrace(self) :
        print("==========================")
        state = self
        while state != None :
            print(state)
            state = state.prev_state

    def find_best(self, verbose = False) :
#        best_score = -1
#        best_state = None
#        for c in self.children :
#            b = c.find_best(verbose)
#            if b.ratio_score() > best_score :
#                best_score = b.ratio_score()
#                best_state = b
#        if best_state == None :
#            return self
#        return best_state

        pass#--print("find_best")
        state = self
        if verbose :
            print(state)
        while len(state.children) > 0:
            max_score = state.children[0].ratio_score()
            max_child = state.children[0]
            for c in state.children:
                c_ratio_s = c.ratio_score()
                if c_ratio_s > max_score:
                    max_score = c_ratio_s
                    max_child = c
            state = max_child
            if verbose :
                print(state)
        return state

def interactive_explore(root) :
    state = root

    while True :
        print ("")
        print (state)
        x = raw_input("which child ? ('b' for back)")
        if x == '' :
            continue
        if x == 'b' :
            if state.prev_state != None :
                state = state.prev_state
                continue
            else:
                return
        try :
            i = int(x)
            if i >= len(state.children) :
                print("out of bounds")
                continue
            state = state.children[i]
        except:
            pass


def mcts() :
    global best_scored

    (width, height) = (5,5) # (7,10)
    tiles = ['#'] * (width * height)
    board = Board(width, height, tiles, [], None)
    print(board)

    visited = set()

    root = State(None, board)

    start_time = millitime()
    iter_count = 0

    allow_interactive = True

    while(millitime() - start_time < 1000 * 60 * 15) :
        # Selection
        selected = root.select()

        # Expansion
        selected.expand(visited)

#        raw_input("raw_input in mcts")

        child = selected.random_child()
        if child != None :
            # Simulation
            leaf = child.simulate(visited)
        else:
            pass# print("Has no children, backpropagating directly")
            parent = selected.prev_state
            if parent != None :
                parent.children.remove(selected)
            leaf = selected

        # Backpropagation
        leaf.backpropagate()

        if (iter_count % 1000 == 0) :
#            print(str(iter_count) + ":\n" + str(root.find_best(False)));
            print(str(iter_count) + ":\n" + str(best_scored));
#            print(str(iter_count) + ":\n" + str(leaf));
            print("explore_count : " + str(stat_explore_count) + ", exploit_count : " + str(stat_exploit_count) + ", ratio : " + str(stat_explore_count / float(stat_exploit_count + 1)))
            if allow_interactive :
                x = raw_input("\n\nexplore ?")
                if len(x) > 0 and x[0] == 'y' :
                    interactive_explore(root)
                if len(x) > 0 and x[0] == 'n' :
                    allow_interactive = False
            #raw_input()

        iter_count += 1
#        raw_input()

#    select = root.find_best(True)
#    select.print_stacktrace()

    best_scored.print_stacktrace()

mcts()
